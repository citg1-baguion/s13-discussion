package com.zuitt.discussion.repositories;

import com.zuitt.discussion.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//Post is the data type of the data used in the methods.
//Object is the data type of the data returned from the database.
//An interface marked as @Repository contains methods for database manipulation
@Repository
public interface UserRepository extends CrudRepository<User, Object> {

//      Custom method to find a user using username

    User findByUsername(String username);

}
