package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;



// Marks this java object as a representation of an entity/record from the database table "posts".
@Entity
//Designate the table name related to the model.
@Table(name = "user")
public class User {

//    Properties
//    Indicates the primary key
    @Id
//    id will be auto-incremented.
    @GeneratedValue
    private Long id;

    @Column
    private String username;
    @Column
    private String password;

    // establishes the relationship of the property to the user model
    @OneToMany(mappedBy = "user")
    // Prevent infinite recursion with bidirectional relationship
    @JsonIgnore
    //Set class, collection that contains no duplicate elements

    private Set<Post> posts;
//    Class properties that represents table column in a relational database are annotated as @Column

//    Constructors
//    Default constructors are required when retrieving data from the database.
    public User() {
    }
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

//    Getters and Setters
    public Long getGetId() {
        return id;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setContent(String password){
        this.password = password;
    }
}
